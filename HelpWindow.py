"""

"""
__author__ = 'CodeBlue'

from PySide import QtGui, QtCore
import datetime as dt


class HelpModel():

    """

    """

    def __init__(self):

        """

        """

        self.title = "Information & Usage"
        self.author = "CodeBlue"
        self.version = "2.0"
        self.__updated = dt.date(year=2013, month=5, day=1)
        self.usage = (
            "Settings:\n"
            "This section goes over the fields used in the settings "
            "section of the application.\n"
            "\n"
            "    The username field is the username login credential "
            "used to access the Pluralsight website from a web "
            "browser.\n"
            "\n"
            "    The password field is the password login credential "
            "used to access the Pluralsight website from a web "
            "browser.\n"
            "\n"
            "The username and password fields are required to use "
            "this application. The values are used to generate the "
            "access token Pluralsight uses to retrieve the course "
            "videos. Because of this, if either of these fields are "
            "missing, the application will not run.\n"
            "\n"
            "    The download location field is the local directory on "
            "the machine to save the downloaded videos to. If this "
            "directory does not exist, the application will create it "
            "before trying to download videos.\n"
            "\n"
            "In addition, the button located next to the download "
            "location text field can be used to navigate to the "
            "desired directory on your machine.\n"
            "\n"
            "    The download exercise files check box is for the "
            "application to determine whether to download exercise "
            "files for a course (if they are available) or not.\n"
            "\n"
            "    The save settings button is to prevent manual entry "
            "of login credentials every time the application is started. "
            "The application saves the settings in a SETTINGS.ini file "
            "in the same directory as the application. Upon starting "
            "the application attempts to read and populate the data "
            "fields with the information from it."
            "\n"
            "There is no encryption on this file so be sure not to give "
            "this information to anyone else.\n"
            "\n"
            "\n"
            "Pluralsight Courses:\n"
            "This section covers the fields used in the Pluralsight "
            "Course Library section of the application.\n"
            "\n"
            "    The filter text box is used to find courses that contain "
            "specific keywords. This should reduce the amount of "
            "searching done through the course tree.\n"
            "\n"
            "    The course tree lists all of the currently available "
            "courses offered by Pluralsight. There are currently "
            "three groupings available. The library grouping lists all of "
            "the courses offered by Pluralsight which are further "
            "grouped by their category. There is also a most popular "
            "grouping and a most recently added grouping."
            "\n"
            "\n"
            "Warnings:\n"
            "This application breaks the Pluralsight Terms of use, "
            "which can can be found on their website, for the "
            "following reasons:\n"
            "5. Prohibited Conduct:\n"
            "    a) Capture, download, save, or retain information and "
            "content available on the Site other than what is "
            "expressly allowed by the Plus, Premium, and Ultimate "
            "subscripition plans.\n"
            "    d) Violate or attempt to violate the Site's security "
            "mechanisms, or otherwise breach the security of the "
            "Site or corrupt the Site in any way. To ensure that users "
            "of the Site do not engage in Prohibited Conduct, "
            "Pluralsight reserves the right to monitor use of the Site "
            "and reserves the right ot revoke or deny access to "
            "users whose usage behavior exceeds normal limits, "
            "suggesting Prohibited Conduct. The term \"normal limits\" "
            "shall be determined solely by Pluralsight.\n"
            "\n"
            "For this reason, the author of this software is not to be "
            "held responsible for the (mis)use of this application or "
            "the accounts used. The end user is aware of the "
            "implications of using this software and will not hold the "
            "author responsible."
        )

    def updated(self):

        """

        """

        return self.__updated.strftime("%m/%d/%y")


class HelpView(QtGui.QDialog):

    """

    """

    def __init__(self, parent=None):

        """

        """

        super(HelpView, self).__init__(parent)
        self.__initializeUI()

        self.setWindowIcon(QtGui.QIcon("./Resources/icons/help.png"))
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setFixedSize(340, 275)

    def __initializeUI(self):

        """

        """

        vBox = QtGui.QVBoxLayout()
        vBox.addWidget(self.__createInformationGroupBox())
        vBox.addWidget(self.__createUsageGroupBox())

        self.setLayout(vBox)

    def __createInformationGroupBox(self):

        """

        """

        informationGroupBox = QtGui.QGroupBox(title="&Application Information:")
        self.__createInformationWidgets(informationGroupBox)
        return informationGroupBox

    def __createInformationWidgets(self, box):

        """

        """

        hBox = QtGui.QHBoxLayout()

        font = QtGui.QFont()
        font.setBold(True)

        lblAuthor = QtGui.QLabel("Author:")
        lblAuthor.setFont(font)
        hBox.addWidget(lblAuthor)
        self.lblAuthor = QtGui.QLabel()
        hBox.addWidget(self.lblAuthor)

        lblVersion = QtGui.QLabel("Version:")
        lblVersion.setFont(font)
        hBox.addWidget(lblVersion)
        self.lblVersion = QtGui.QLabel()
        hBox.addWidget(self.lblVersion)

        lblUpdated = QtGui.QLabel("Updated:")
        lblUpdated.setFont(font)
        hBox.addWidget(lblUpdated)
        self.lblUpdated = QtGui.QLabel()
        hBox.addWidget(self.lblUpdated)

        box.setLayout(hBox)

    def __createUsageGroupBox(self):

        """

        """

        usageGroupBox = QtGui.QGroupBox(title="&Usage Details:")
        self.__createUsageWidgets(usageGroupBox)
        return usageGroupBox

    def __createUsageWidgets(self, box):

        """

        """

        self.txtUsage = QtGui.QTextEdit()
        self.txtUsage.setReadOnly(True)

        hBox = QtGui.QHBoxLayout()
        hBox.addWidget(self.txtUsage)

        box.setLayout(hBox)


class HelpController():

    """

    """

    def __init__(self, parent=None):

        """

        """

        self.__model = HelpModel()
        self.__view = HelpView(parent)

        self.initializeView()

    def initializeView(self):

        """

        """

        self.__view.lblAuthor.setText(self.__model.author)
        self.__view.lblVersion.setText(self.__model.version)
        self.__view.lblUpdated.setText(self.__model.updated())
        self.__view.txtUsage.setText(self.__model.usage)

        self.__view.setWindowTitle(self.__model.title)
        self.__view.show()