"""
Classes and methods related to the creation and display of the MainWindow graphical user interface.

Contains the Model, View, and Controller classes and methods related to the creation, display and interaction with the
MainWindow graphical user interface.

The Model class is used to store and load data utilized by the graphical user interface of the MainWindow.

The View class is used to display the Model's data to the user in a graphical user interface.

The Controller class is used to initiate the model and the view and to listen for user activity and respond accordingly.

"""
__author__ = 'CodeBlue'

import sys
import ConfigParser
import os
from PySide import QtGui, QtCore
from HelpWindow import HelpController


class SettingsModel():

    """
    The SettingsModel class is used to store and load data utilized by the graphical user interface of the MainWindow
    into the SETTINGS.ini file in the root directory.

    The SettingsModel class stores the settings in a file designated by the variable '__CONFIG_FILE' by writing the
    local global variables to the header section designated by the variable '__LOGIN_SECTION'.

    This requires the instance variables be updated before calling the saveSettings function. The MainWindow model
    contains instance variables for a username, password, download location, and download exercise files. The username
    property is the username login credential used to access Pluralsight from a web browser.

    In addition the SettingsModel class contains the private instance variables __CONFIG_FILE and __LOGIN_SECTION.
    The __CONFIG_FILE instance variable holds the name of the configuration file where settings are read from and stored
    to. The __LOGIN_SECTION holds the name of the section within the configuration file where these settings are stored.
    """

    __CONFIG_FILE = "./SETTINGS.ini"
    __LOGIN_SECTION = "LOGIN_SETTINGS"

    def __init__(self):

        """
        Initializes the SettingsModel properties.

        Initializes the SettingsModel properties by calling the private helper method '__initializeSettings()'.
        """

        self.__initializeSettings()

    def __initializeSettings(self):

        """
        Initializes the SettingsModel properties by reading the file specified by the property '__CONFIG_FILE' using the
        ConfigParser library.

        Initializes the SettingsModel properties by reading the file specified by the property '__CONFIG_FILE' using the
        ConfigParser library if it exists. Otherwise the function calls the private helper method '__createConfig()'
        to create the config file with default values.
        """

        config = ConfigParser.ConfigParser()
        config.read(self.__CONFIG_FILE)

        if not os.path.exists(self.__CONFIG_FILE) or not config.has_section(self.__LOGIN_SECTION):
            self.__createConfig()

        config.read(self.__CONFIG_FILE)
        self.username = config.get(self.__LOGIN_SECTION, 'Username')
        self.password = config.get(self.__LOGIN_SECTION, 'Password')
        self.downloadLocation = config.get(self.__LOGIN_SECTION, 'DownloadLocation')
        self.downloadExerciseFiles = config.get(self.__LOGIN_SECTION, 'DownloadExerciseFiles')

    def __createConfig(self):

        """
        Creates the configuration file specified by the property '__CONFIG_FILE' using the ConfigParser library.

        Creates the configuration file specified by the property '__CONFIG_FILE' using the ConfigParser libary.
        If the the section specified by the property '__LOGIN_SECTION' does not exist it creates it and populates this
        section with default values.
        """

        config = ConfigParser.ConfigParser()
        config.read(self.__CONFIG_FILE)

        if not config.has_section(self.__LOGIN_SECTION):
            config.add_section(self.__LOGIN_SECTION)

        config.set(self.__LOGIN_SECTION, 'Username', '""')
        config.set(self.__LOGIN_SECTION, 'Password', '""')
        config.set(self.__LOGIN_SECTION, 'DownloadLocation', '""')
        config.set(self.__LOGIN_SECTION, 'DownloadExerciseFiles', False)

        with open(self.__CONFIG_FILE, 'w+') as cfgFile:
            config.write(cfgFile)
            cfgFile.close()

    def saveSettings(self):

        """
        Save the SettingsModel properties to the file specified by the property '__CONFIG_File' using the ConfigParser
        library.
        """

        config = ConfigParser.RawConfigParser()

        config.read(self.__CONFIG_FILE)
        config.set(self.__LOGIN_SECTION, 'Username', self.username)
        config.set(self.__LOGIN_SECTION, 'Password', self.password)
        config.set(self.__LOGIN_SECTION, 'DownloadLocation', self.downloadLocation)
        config.set(self.__LOGIN_SECTION, 'DownloadExerciseFiles', self.downloadExerciseFiles)

        with open(self.__CONFIG_FILE, 'w+') as cfgFile:
            config.write(cfgFile)
            cfgFile.close()


class LibraryModel(QtCore.QAbstractItemModel):

    """

    """

    def __init__(self):
        super(LibraryModel, self).__init__()


class MainView(QtGui.QMainWindow):

    """
    The MainView class derives from the QtGui.QMainWindow class and is used to display a graphical user interface to
    users who run the application.

    The MainView class derives from the QtGui.QMainWindow class and is used to display a graphical user interface to
    users who run the application.

    The MainView class also exposes the widgets that interact with the user so the MainController class may reference
    and connect to the signals issued from these widgets. The instance variables are:
        btnBrowseDirectories -- Button widget used to browse for a directory to use as the download location.
        btnSaveSettings -- Button widget used to save settings the user has input for logging in, selecting a download
        location, or whether the application should download exercise files if they are available.
        chkBoxDownloadExerciseFiles -- Checkbox widget used to determine if the application should download exercise
        files from Pluralsight if they are available.
        exitAction -- the action taken when the user pushes the exit icon in the file menu, toolbar, or shortcut key
        specified (Ctrl+Q)
        helpAction -- the action taken when the user pushes the help icon in the file menu, toolbar, or shortcut key
        specified (F1)
        startAction -- the action taken when the user pushes the start icon in the file menu, toolbar, or shortcut key
        specified (HOME)
        stopAction -- the action taken when the user pushes the stop icon in the file menu, toolbar, or shortcut key
        specified (END)
        treeCourses -- the widget to display the tree of available courses offered by Pluralsight.
        txtDownloadLocation -- the textbox widget used to store the download location specified by the user.
        txtFilterCourses -- the textbox widget used so the user can filter courses to a subset
        txtPassword -- the textbox widget used to store the Pluralsight password specified by the user.
        txtUsername -- the textbox widget used to store the Pluralsight username specified by the user.
    """

    def __init__(self, parent):

        """

        """

        super(MainView, self).__init__(parent)
        self.__initializeUI()

    def __initializeUI(self):

        """

        """

        self.__createActions()
        self.__createFileMenu()
        self.__createToolBar()

        self.setCentralWidget(self.__createCentralWidget())

        self.setWindowIcon(QtGui.QIcon("./Resources/icons/application.png"))
        self.setWindowTitle("Pluralsight Course Downloader")
        self.setFixedSize(450, 450)

    def __createActions(self):

        """

        """

        self.startAction = QtGui.QAction(QtGui.QIcon("./Resources/icons/start.png"), "&Start", self)
        self.startAction.setToolTip("(HOME): Start the application.")
        self.startAction.setShortcut("Home")

        self.stopAction = QtGui.QAction(QtGui.QIcon("./Resources/icons/stop.png"), "&Stop", self)
        self.stopAction.setToolTip("(END): Stop the application.")
        self.stopAction.setShortcut("End")

        self.exitAction = QtGui.QAction(QtGui.QIcon("./Resources/icons/exit.png"), "&Exit", self)
        self.exitAction.setToolTip("(Alt+X): Exit the application.")
        self.exitAction.setShortcut("Alt+X")

        self.helpAction = QtGui.QAction(QtGui.QIcon("./Resources/icons/help.png"), "&Help", self)
        self.helpAction.setToolTip("(F1): Additional usage resources.")
        self.helpAction.setShortcut("F1")

    def __createFileMenu(self):

        """

        """

        _fileMenu = self.menuBar().addMenu("&File")
        _fileMenu.addAction(self.startAction)
        _fileMenu.addAction(self.stopAction)
        _fileMenu.addSeparator()
        _fileMenu.addAction(self.exitAction)

        _helpMenu = self.menuBar().addMenu("&Help")
        _helpMenu.addAction(self.helpAction)

    def __createToolBar(self):

        """

        """

        self.toolBar = self.addToolBar("&ToolBar")
        self.toolBar.addAction(self.exitAction)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.startAction)
        self.toolBar.addAction(self.stopAction)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.helpAction)

    def __createCentralWidget(self):

        """

        """

        centralWidget = QtGui.QWidget()

        vBox = QtGui.QVBoxLayout()
        vBox.addWidget(self.__createSettingsGroupBox())
        vBox.addWidget(self.__createCourseGroupBox())

        centralWidget.setLayout(vBox)
        return centralWidget

    def __createSettingsGroupBox(self):

        """

        """

        settingsGroupBox = QtGui.QGroupBox()
        self.__createSettingsWidgets(settingsGroupBox)
        settingsGroupBox.setTitle("&Settings:")
        return settingsGroupBox

    def __createSettingsWidgets(self, box):

        """

        """

        grid = QtGui.QGridLayout()
        grid.setSpacing(5)

        grid.addWidget(QtGui.QLabel("Username:"), 0, 0, 1, 3)
        self.txtUsername = QtGui.QLineEdit()
        grid.addWidget(self.txtUsername, 0, 3, 1, 3)
        grid.addWidget(QtGui.QLabel("Password:"), 0, 6, 1, 3)
        self.txtPassword = QtGui.QLineEdit()
        self.txtPassword.setEchoMode(QtGui.QLineEdit.Password)
        grid.addWidget(self.txtPassword, 0, 9, 1, 3)

        grid.addWidget(QtGui.QLabel("Download Location:"), 1, 0, 1, 4)
        self.txtDownloadLocation = QtGui.QLineEdit()
        grid.addWidget(self.txtDownloadLocation, 1, 4, 1, 7)
        self.btnBrowseDirectories = QtGui.QPushButton(QtGui.QIcon("./Resources/icons/browse.png"), "")
        grid.addWidget(self.btnBrowseDirectories, 1, 11)

        self.chkBoxDownloadExerciseFiles = QtGui.QCheckBox("Download Exercise Files?")
        grid.addWidget(self.chkBoxDownloadExerciseFiles, 2, 0, 1, 4)
        self.btnSaveSettings = QtGui.QPushButton(QtGui.QIcon("./Resources/icons/save.png"), "Save Settings")
        grid.addWidget(self.btnSaveSettings, 2, 8, 1, 4)

        box.setLayout(grid)

    def __createCourseGroupBox(self):

        """

        """

        pluralsightCoursesGroupBox = QtGui.QGroupBox()
        self.__createCourseWidgets(pluralsightCoursesGroupBox)
        pluralsightCoursesGroupBox.setTitle("&Pluralsight Course Library")
        return pluralsightCoursesGroupBox

    def __createCourseWidgets(self, box):

        """

        """

        grid = QtGui.QGridLayout()
        grid.setSpacing(5)

        self.txtFilterCourses = QtGui.QLineEdit()
        grid.addWidget(QtGui.QLabel("Filter:"))
        grid.addWidget(self.txtFilterCourses, 0, 1)

        self.treeCourses = QtGui.QTreeView()
        grid.addWidget(self.treeCourses, 1, 0, 1, 2)

        box.setLayout(grid)


class MainController:

    """
    The MainController class is used to listen for user activity and respond accordingly.
    """

    def __init__(self, app=None):

        """

        """

        self.__helpWindow = None
        self.__settings = SettingsModel()
        self.__library = LibraryModel()

        self.__initializeView()

    def __initializeView(self):

        """

        """

        self.__view = MainView(None)
        self.__view.stopAction.setEnabled(False)

        self.__view.txtUsername.setText(self.__settings.username)
        self.__view.txtPassword.setText(self.__settings.password)
        self.__view.txtDownloadLocation.setText(self.__settings.downloadLocation)
        self.__view.chkBoxDownloadExerciseFiles.setChecked(self.__settings.downloadExerciseFiles == 'True')

        self.__view.startAction.triggered.connect(self.__start)
        self.__view.stopAction.triggered.connect(self.__stop)
        self.__view.exitAction.triggered.connect(self.__view, QtCore.SLOT("close()"))
        self.__view.helpAction.triggered.connect(self.__showHelp)

        self.__view.btnBrowseDirectories.clicked.connect(self.__showDirectoryDialog)
        self.__view.btnSaveSettings.clicked.connect(self.__saveSettings)

        __proxyModel = QtGui.QSortFilterProxyModel()

        self.__view.treeCourses.setModel(__proxyModel)
        self.__view.treeCourses.setSortingEnabled(True)

        QtCore.QObject.connect(self.__view.txtFilterCourses, QtCore.SIGNAL("textChanged(QString)"),
                               __proxyModel.setFilterRegExp)

        self.__view.setStatusTip('Ready')
        self.__view.show()

    def __start(self):

        """

        """

        self.__view.statusBar().showMessage("Start not implemented yet.")

    def __stop(self):

        """

        """

        self.__view.statusBar().showMessage("Stop not implemented yet.")

    def __showHelp(self):

        """

        """
        if self.__helpWindow is None:
            self.__helpWindow = HelpController(self.__view)
        else:
            self.__helpWindow.initializeView()

        self.__view.statusBar().showMessage("Loading Information & Usage Dialog.")

    def __showDirectoryDialog(self):

        """

        """

        txtSelectedDirectory = QtGui.QFileDialog.getExistingDirectory(caption='Select Download Location')
        self.__view.txtDownloadLocation.setText(txtSelectedDirectory)

    def __saveSettings(self):

        """

        """

        self.__settings.username = self.__view.txtUsername.text() \
            if self.__view.txtUsername.text() != '' else '""'

        self.__settings.password = self.__view.txtPassword.text() \
            if self.__view.txtPassword.text() != '' else '""'

        self.__settings.downloadLocation = self.__view.txtDownloadLocation.text() \
            if self.__view.txtDownloadLocation.text() != '' else '""'

        self.__settings.downloadExerciseFiles = self.__view.chkBoxDownloadExerciseFiles.isChecked()

        self.__settings.saveSettings()
        self.__view.statusBar().showMessage("Settings saved.")


def main():

    """

    """

    app = QtGui.QApplication(sys.argv)
    controller = MainController()
    app.exec_()

if __name__ == '__main__':
    main()