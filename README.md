This document is an introduction to the Pluralsight Course Downloader,
which as its title suggests downloads videos from Pluralsight assuming
you have a valid login with them.

Introduction:
============
Pluralsight Course Downloader is a python program, utilizing the Qt
framework, that will download selected videos from Pluralsight to a
specified user directory. The only requirements for this application
to work is to have a valid login with Pluralsight currently and an
internet connection.

Getting Started:
---------------
To use Pluralsight Course Downloader, run the application, enter the
Pluralsight login credentials, select a download location, then select
courses from the Pluralsight library to download. Once finished, push
the start application.

Warnings:
============
Pluralsight does have securities in place to IP ban if too many videos
are watched or downloaded in a certain amount of time. This program also
violates their terms and conditions by downloading the videos. You use
this program at your own risk, which may result in the loss of your
account or other legal implications. I cannot be held responsible for the
actions taken by Pluralsight.